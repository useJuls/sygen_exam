import React, { useState, useEffect, useContext, Fragment } from 'react';
import AlbumService from '../services/album.service';
import { AlbumContext } from '../context/album/provider';
import { types } from '../context/album/actions';
import Pagination from '../components/Pagination';

const AlbumHeader = props => {
  
  const pages = [25, 50, 100, 250, 500];
  const { triggerModal } = props;
  const context = useContext(AlbumContext);
  const garbage = context.state.garbage;

  const [loading, setLoading] = useState(false)
  
  useEffect(() => {
    context.dispatch({  
      type: types.SET_PAGE_SIZE,
      payload: pages[0]
    })
    getPhotos({ 
      skip: 0,
      limit: pages[0]
    });
  }, [])

  function onLimitClicked(page) {
    context.dispatch({  
      type: types.SET_PAGE_SIZE,
      payload: page
    })
    getPhotos({ 
      skip: 0,
      limit: page
    });
  } 

  async function checkState() {
    console.log(context.state);
  }
  
  async function deletePhotos() {
    setLoading(true)
    
    try {
      const payload = context.state.garbage;
      const res = await AlbumService.deleteImages(payload);

      if (res.status === 200) {
        setLoading(false)
        showToasty('Photos Successfully Deleted', 'success')
        context.dispatch({
          type: types.DELETE_MANY,
          payload
        })
      }      
    } catch (error) {
      setLoading(false)
      showToasty('Something went wrong', 'error')
    }
  }

  function clearSelectedImages() {
    context.dispatch({
      type: types.CLEAR_GARBAGE
    })
  }

  async function getPhotos(params) {
    setLoading(true)
    try {
      const res = await AlbumService.listPhotos(params)
      if (res.status === 200) {
        setLoading(false)
        
        context.dispatch({
          type: types.SET_PHOTOS,
          payload: {
            photos: res.data.documents,
            total: res.data.total
          }
        });
      }
    } catch (error) {
      setLoading(false)
      console.log(error)
    }
  }

  function openModal() {
    triggerModal(true)
    context.dispatch({
      type: types.CLEAR_GARBAGE
    })
  }

  function showToasty(message, toast_type) {
    context.dispatch({
      type: types.SET_TOAST_VISIBLE,
      payload: {
        toastVisible: true,
        message,
        toast_type,
      }
    })
  }

  return(
    <div
      style={
        context.state.toastVisible ? { 'top': '1.9rem' } : { 'top': '0' } 
      } 
      className="container main-wrapper__headers">
      
      <div className="row w-100">
        <div className="col-md-4">
          <div className="title-main-wrapper">
            {
              loading && (
                <div className="spinner-border mr-2" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              )
            }
            <span className="title-wrapper">
              <p 
                onClick={ checkState }
                className="title mb-0">
                Photos
              </p>
              <small>{ context.state.total } results</small>
            </span>
          </div>
        </div>
     

        <div className="col-md-8 pr-0">
        <div className="action-buttons-wrapper">
          <div className="action-buttons">
            {
              garbage.length !== 0 && (
                <Fragment>
                  <button
                    onClick={ deletePhotos } 
                    className="btn text-dark py-0 border-right">
                    <i className="fa fa-trash mr-1"></i> 
                    Delete { garbage.length } photos
                  </button>

                  <button
                    onClick={ clearSelectedImages } 
                    className="btn text-dark py-0 border-right"> 
                    Clear Selected Images
                  </button>
                </Fragment>
              )
            }

            <button
              onClick={ openModal } 
              className="btn text-dark py-0 border-right">
              <i className="fa fa-paperclip mr-1 "></i> Upload
            </button>
            
            <div className="dropdown">
              <button className="btn d-flex align-items-center py-0">
                { context.state.page_size } 
                <i className="fa fa-chevron-down"></i>
              </button>
              <ul className="dropdown-content">
                {
                  pages.map((page, i) => (
                    <li
                      key={ i }
                      onClick={ () => onLimitClicked(page) }  
                      className="dropdown-content__item">
                      { page }
                    </li>
                  ))
                }
              </ul>
            </div>
          </div>

          <Pagination setLoading={ setLoading } />
        </div>
      </div>
      
      </div>
    </div>
  )
}

export default AlbumHeader;