import React, { Fragment, useState, useEffect, useContext } from 'react';
import AlbumHeader from './AlbumHeader';
import AlbumModal from './Modal';
import ViewImage from './ViewImage';
import Image from './Image';
import Toasty from './Toasty';
import { AlbumContext } from '../context/album/provider.js';
import { types } from '../context/album/actions';
import '../styles/_main.scss'

const Home = () => {

  const [modalIsVisible, setModalIsVisible] = useState(false);
  const [viewIsVisible, setViewIsVisible] = useState(false);
  const [selectedImage, setSelectedImage] = useState({})
  const context = useContext(AlbumContext);
  const photos = context.state.photos;

  useEffect(() => {
    setTimeout(() => {
      context.dispatch({
        type: types.SET_TOAST_VISIBLE,
        payload: {
          toastVisible: false
        }
      })
    }, 3000)
  }, [context.state.toastVisible]) 

  function _buildImages(photos) {
    return (
      photos.map((photo, index) => <Image
        setViewIsVisible={ setViewIsVisible }
        setSelectedImage={ setSelectedImage }
        key={ index }
        src={ photo.path }
        name={ photo.name }
        album={ photo.album }  
      />)
    ) 
  }

  function _buildEmptyImages() {
    return (
      <p className="text-dark">No Images Found.</p>
    )
  }
  
  return (
    <Fragment>
      <div className="container-fluid main-wrapper">

        <Toasty />

        <AlbumHeader 
          triggerModal={ setModalIsVisible }
        />

        <AlbumModal 
          isVisible={ modalIsVisible }
          triggerModal={ setModalIsVisible }  
        />

        <ViewImage 
          isVisible={ viewIsVisible }
          selectedImage={ selectedImage }
          setViewIsVisible={ setViewIsVisible }
        />

        <div className="container">
          <div className="row mt-5">
            {
              photos && photos.length !== 0 
                ? _buildImages(photos)
                : _buildEmptyImages()
            }
          </div>
        </div>
      </div>      

    </Fragment>
  );
}

export default Home;