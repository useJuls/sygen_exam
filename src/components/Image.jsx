import React, { useState, useContext, useEffect } from 'react';
import cx from 'classnames';
import { AlbumContext } from '../context/album/provider';
import { types } from '../context/album/actions';

const Image = props => {
  
  const context = useContext(AlbumContext);
  const garbage = context.state.garbage;
  const [isChecked, setIsChecked] = useState(false) 
  const { src, album, name, setViewIsVisible, setSelectedImage  } = props;
  const payload = {
    album,
    documents: name
  }

  useEffect(() => {
    checkIfExists()

    return function cleanUp() {
      setIsChecked(false)
    }
  }, [garbage])

  function checkIfExists() {
    const doesExists = garbage.find(g => g.documents === name)
    if (doesExists) 
      setIsChecked(true)
  }

  function onCheckClicked() {
    context.dispatch({
      type: types.ADD_TO_GARBAGE,
      payload
    })
  }

  function onImageClicked() {
    setViewIsVisible(true)
    setSelectedImage({
      src, album, name
    })
  }

  function trimName(string) {
    if (string.length > 30) {
      return string.substring(0, 25) + '...'
    } else {
      return string
    }
  }

  return (
    <div
      className="col-md-3 col-6">
      <div className="image-container">
        <div 
          className={ cx('hover-actions', {
            'visible': isChecked
          }) }>
          <input 
            type="checkbox"
            onChange={ onCheckClicked }
            checked={ isChecked }
            className="form-check-input" />
        </div>
        
        <div 
          onClick={ onImageClicked }
          className="image-wrapper">
          <img 
            src={ src } 
            alt={ `${album}/${name}` } 
            className="image"/>
        </div>

        <p className="font-weight-bold mb-0 mt-2">{ trimName(name) }</p>
        <p className="text-dark">{ album }</p>
      </div>
    </div>
  )
}

export default Image;