import React, { useState, useEffect, useContext } from 'react';
import cx from 'classnames';
import AlbumService from '../services/album.service';
import { useDropzone } from 'react-dropzone';
import { AlbumContext } from '../context/album/provider';
import { types } from '../context/album/actions';

const AlbumModal = props => {
  const { isVisible, triggerModal } = props;
  const albums = ['Travel', 'Personal', 'Food', 'Nature', 'Other'];
  const context = useContext(AlbumContext)

  const [photos, setPhotos] = useState([]);
  const [loading, setLoading] = useState(false);
  const [selectedAlbum, setSelectedAlbum] = useState('');
  const [files, setFiles] = useState([]);

  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    onDrop: acceptedFiles => {
      setPhotos(acceptedFiles);
      setFiles(acceptedFiles.map(file => Object.assign(file, {
          preview: URL.createObjectURL(file)
        }))
      );
    }
  });

  function hideModal() {
    triggerModal(false);
    setFiles([]);
    setPhotos([])
    setSelectedAlbum('')
  }
  
  useEffect(() => () => {
    files.forEach(file => {
      URL.revokeObjectURL(file.preview)
    });
  }, [files]);

  function removeImage(path) {
    const images = files.filter(file => file.path != path);
    setFiles(images);
    setPhotos(images);
  }

  async function upload() {
    console.log('upload triggered');
    const filtered_images = [];
    const accepted_files = ['image/png', 'image/jpg', 'image/jpeg'];

    photos.map(photo => {
      console.log(photo.type)
      if (!accepted_files.includes(photo.type)) {
        filtered_images.push(photo)
      }
    });
     
    if (filtered_images.length !== 0) {
      return showToasty('Please provide only jpg/jpeg & png images', 'error');

    } else {

      try {
        setLoading(true)
        const formData = new FormData();
        formData.append('album', selectedAlbum);
        photos.map(photos => formData.append('documents', photos));
  
        const res = await AlbumService.upload(formData);
        if (res.status === 200) {
          setLoading(false)
          hideModal()
          showToasty('Image(s) Successfully Uploaded!', 'success')
          context.dispatch({
            type: types.ADD_TO_PHOTOS,
            payload: res.data.data
          })
        }
      } catch (error) {
        setLoading(false)
        console.log(error)
        showToasty('Something went wrong.', 'error');
      }
    }

  }

  function showToasty(message, toast_type) {
    context.dispatch({
      type: types.SET_TOAST_VISIBLE,
      payload: {
        toastVisible: true,
        message,
        toast_type,
      }
    })
  }

  const thumbs = files.map(file => (
    <div className="thumb" key={file.name}>

      <div
        onClick={ () => removeImage(file.path) }
        className="delete-button">
        <span className="times">&times;</span>
      </div>

      <div className="thumb-inner">
        <img
          className="thumb-image"
          src={file.preview}
        />
      </div>
    </div>
  ));

  return (
    <div className={ cx('modal', {
      'd-none': !isVisible
    }) }>
      <div className="modal-content">
        <div className="modal-content__header">

          <p className="modal-content__title">
            Upload photos
          </p>

          <span
            onClick={ hideModal } 
            className="close">&times;
          </span>

        </div>
        
        <section>
          <div {...getRootProps({className: 'dropzone'})}>
            <input {...getInputProps()} />
            <p className="mb-0">Drag 'n' drop some files here, or click to select files</p>
          </div>

          {
            files.length === 0 && (
              <p className="no-file-message">No files selected...</p>
            )
          }

          <aside className="thumb-container">
            {thumbs}
          </aside>
        </section>

        <div className="action-buttons mt-3 d-flex justify-content-between">

          <div className="dropdown">
            <button
              className="pt-2 btn d-flex align-items-center">
              { selectedAlbum === '' ? 'Select album' : selectedAlbum }
              <i className="fa fa-chevron-down"></i>
            </button>
            <ul className="dropdown-content">
              {
                albums.map((album, i) => (
                  <li
                    key={ i }
                    onClick={ () => setSelectedAlbum(album) }  
                    className="dropdown-content__item">
                    { album }
                  </li>
                ))
              }
            </ul>
          </div>

          <button
            onClick={ upload }
            className={ cx('btn d-flex align-items-center', {
              'not-allowed': photos.length === 0 || selectedAlbum === ''
            }) }
            disabled={ photos.length === 0 || selectedAlbum === '' }>
            {
              loading && (
                <div className="spinner-border mr-2" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              )
            }
            Upload
          </button>
        </div>

      </div>
    </div>
  )
}

export default AlbumModal;