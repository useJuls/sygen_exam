import React, { useState, useContext, useEffect } from 'react'
import cx from 'classnames';
import AlbumService from '../services/album.service';
import { AlbumContext } from '../context/album/provider';
import { types } from '../context/album/actions';

const Pagination = props => {
  
  const context = useContext(AlbumContext)

  const { page_size, total } = context.state;
  const { setLoading } = props;

  const [currentPage, setCurrentPage] = useState(1);
  const [offset, setOffset] = useState(0)
  const [pageCount, setPageCount] = useState(0)

  useEffect(() => {
    const count = Math.ceil(total/page_size)
    setPageCount(count);

  }, [context.state])

  useEffect(() => {
    onPaginationClick(currentPage, {
      skip: ( currentPage - 1 ) * page_size,
      limit: page_size
    })
  }, [currentPage])

  async function onPaginationClick(index, params) {
    setLoading(true)
    setCurrentPage(index)
    try {
      const res = await AlbumService.listPhotos(params)
      if (res.status == 200) {
        setLoading(false)
        
        context.dispatch({
          type: types.SET_PHOTOS,
          payload: {
            photos: res.data.documents,
            total: res.data.total
          }
        });
      }
    } catch (error) {
      setLoading(false)
    }
  }

  function _buildPaginationItem() {
    const items = [];
    
    for (let i = 1; i <= pageCount; i++) {
      items.push(
        <li
          onClick={ () => onPaginationClick(i, {
            skip: ( i - 1 ) * page_size,
            limit: page_size
          }) } 
          key={ i } 
          className="page-item">
          <a className={ cx("page-link", {
            "text-white bg-secondary font-weight-bold": i == currentPage,
            "text-dark": i !== currentPage
          }) } href="#">{ i }</a>
        </li>
      )
    }
    return items;
  }
  
  return ( 
    <nav aria-label="Page navigation">
      <ul className="pagination pagination-sm mb-0">
        <li 
          onClick={ () => setCurrentPage(currentPage - 1) } 
          className={ cx("page-item", {
            'blur': currentPage === 1
          }) }>
          <button
            disabled={ currentPage === 1 }
            className="page-link text-dark"
            href="#">
            Previous
          </button>
        </li>
        {
          _buildPaginationItem()
        }
        <li 
          onClick={ () => setCurrentPage(currentPage + 1) } 
          className={ cx("page-item", {
            'blur': currentPage === pageCount
          }) }>
          <button
            disabled={ currentPage === pageCount }
            className="page-link text-dark"
            href="#">
            Next
          </button>
        </li>

      </ul>
    </nav>
  )
}

export default Pagination;