import React, { useContext } from 'react'
import cx from 'classnames';
import { AlbumContext } from '../context/album/provider';
import { types } from '../context/album/actions';


const Toasty = props => {
  const context = useContext(AlbumContext);
  const { toast_type, message, toastVisible } = context.state;

  function hideToasty() {
    context.dispatch({
      type: types.SET_TOAST_VISIBLE,
      payload: {
        toastVisible: false
      }
    })  
  }

  return (
    <div className={ cx("toasty-container", {
      "d-none": !toastVisible,
      "bg-success": toast_type == 'success',
      "bg-danger": toast_type == 'error'
    }) }>
      <div className="d-flex justify-content-center align-items-center">
        {
          toast_type == 'success' 
          ? <i
              className="fa fa-check-circle mr-2"
              aria-hidden="true"></i> 
          : <i 
              className="fa fa-exclamation-circle mr-2"
              aria-hidden="true"></i>
        }
        
        <p className="mb-0">{ message }</p>

        <div className="okay-button">
          <button 
            onClick={ hideToasty }
            className="btn btn-sm text-white">
            okay
          </button>
        </div>
        
      </div>
    </div>
  );
}

export default Toasty;