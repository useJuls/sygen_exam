import React, { useContext, useState } from 'react';
import AlbumService from '../services/album.service';
import cx from 'classnames';
import { AlbumContext } from '../context/album/provider';
import { types } from '../context/album/actions';

const ViewImage = props => {

  const { isVisible, setViewIsVisible, selectedImage } = props;
  const [loading, setLoading] = useState(false);
  const context = useContext(AlbumContext)

  async function deleteImage() {
    const { name, album } = selectedImage;
    setLoading(true);

    try {
      const res = await AlbumService.deletePhoto(album, name)
      
      if (res.status == 200) {
        setLoading(false)
        setViewIsVisible(false)
        showToasty('Image Successfully Deleted', 'success')
        context.dispatch({
          type: types.DELETE_SINGLE,
          payload: name
        })
      }
    } catch (error) {
      console.log(error)
      showToasty('Something went wrong.', 'error')
    }
  }

  function showToasty(message, toast_type) {
    context.dispatch({
      type: types.SET_TOAST_VISIBLE,
      payload: {
        toastVisible: true,
        message,
        toast_type,
      }
    })
  }

  return (
    <div className={ cx('modal', {
      'd-none': !isVisible
    }) }>
      <div className="modal-content">
        <div className="modal-content__header">
          <p className="modal-content__title">
            { selectedImage?.name }
          </p>

         <span
            onClick={ () => setViewIsVisible(false) } 
            className="close">&times;
          </span>
        </div>

        
        <p className="font-weight-bold">
          Album: { selectedImage?.album }
        </p>

        <div className="modal-content__view-image-wrapper">
          <img
            className="image-preview"
            src={ selectedImage?.src }
            alt={ selectedImage?.name }
          />
        </div>

        <hr/>

        <div className="text-right">
          <button 
            onClick={ deleteImage }
            className="btn btn-outline-danger">
            Delete
          </button>
        </div>

      </div>
    </div>
  )
}

export default ViewImage;