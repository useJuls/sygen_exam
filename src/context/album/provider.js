import React, { createContext, useReducer } from 'react'
import { albumReducer } from './reducer'

const initalState = {
  total: 0,
  page_size: 0,
  photos: [],
  garbage: [],

  // toast
  toastVisible: false,
  message: 'message',
  toast_type: 'success'
}

const AlbumContext = createContext();

const AlbumProvider = props => {
  const [state, dispatch] = useReducer(albumReducer, initalState);
  return (
    <AlbumContext.Provider value={{ state, dispatch }}>
      { props.children }
    </AlbumContext.Provider>
  )
}

export {
  AlbumContext,
  AlbumProvider
}