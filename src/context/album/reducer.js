import { types } from './actions';

export const albumReducer = (state, { type, payload }) => {
  switch(type) {

    // INITIALIZE PHOTOS
    case types.SET_PHOTOS: {
      const data = Object.assign({}, state, {
        photos: payload.photos,
        total: payload.total
      })
      return data;
    }
    
    case types.SET_PAGE_SIZE: {
      const data = Object.assign({}, state, {
        page_size: payload
      })

      return data;
    }

    // SELECT PHOTOS TO DELETE
    case types.ADD_TO_GARBAGE: {
     
      let data;
      const { garbage } = state;
      const doesExists = garbage.find(g => g.documents == payload.documents);
            
      if (doesExists == null) {
        data = Object.assign({}, state, {
          garbage: [...garbage, payload]
        })

      } else {
        data = Object.assign({}, state, {
          garbage: garbage.filter(g => g.documents != payload.documents)
        })
      }

      return data;
    }

    // OPTIMISTIC UPDATE
    case types.ADD_TO_PHOTOS: {
      const data = Object.assign({}, state, {
        photos: [...payload, ...state.photos],
        total: state.total + payload.length
      })
      return data;
    }

    // DELETE SINGLE
    case types.DELETE_SINGLE: {
      const data = Object.assign({}, state, {
        photos: state.photos.filter(p => p.name != payload),
        garbage: [],
        total: state.total - 1
      })
      return data;
    }

    // DELETE MANY
    case types.DELETE_MANY: {
      const container = [];
      const remaining = [];
      payload.map(i => container.push(i.documents));

      state.photos.map(photo => {
        if (!container.includes(photo.name)) {
          remaining.push(photo)
        }
      })

      const data = Object.assign({}, state, {
        photos: remaining,
        garbage: [],
        total: state.total - container.length
      })
      
      return data;
    }

    case types.CLEAR_GARBAGE: {
      const data = Object.assign({}, state, {
        garbage: []
      })
      return data;
    }

    case types.SET_TOAST_VISIBLE: {
      const data = Object.assign({}, state, {
        toastVisible: payload.toastVisible,
        message: payload.message,
        toast_type: payload.toast_type
      })

      return data;
    }

  }
}