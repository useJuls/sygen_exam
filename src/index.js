import React from 'react';
import ReactDOM from 'react-dom';
import Home from './components/Home';
import { AlbumProvider } from './context/album/provider';

ReactDOM.render(
  <React.StrictMode>
    <AlbumProvider>
      <Home />
    </AlbumProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
