import axios from 'axios';

class AlbumService {

  constructor() {
    this.endpoint = 'http://localhost:8888'
  }

  async healthCheck() {
    const response = await axios.get(`${this.endpoint}/health`);
    return response;
  };

  async listPhotos(params) {
    const { skip, limit } = params;

    const response = await axios.post(`${this.endpoint}/photos/list`, {
      skip, limit
    });
    return response;
  }

  async upload(formData) {
    const response = await axios.put(`${this.endpoint}/photos`, formData,);
    return response;
  }

  async deletePhoto(albumName, fileName) {
    const response = 
      await axios.delete(`${this.endpoint}/photos/${albumName}/${fileName}`);
    return response;
  }

  async getImage(albumName, fileName) {
    const response = await axios.delete(`${this.endpoint}/photos/${albumName}/${fileName}`);
    return response;
  }

  async deleteImages(photos) {
    const response = await axios({
      method: 'DELETE',
      url: `${this.endpoint}/photos`,
      data: photos
    })
    return response;
  }

}

export default new AlbumService();